import { Router } from "express";
import { verifyToken } from "../libs/valitoken";
import { getPaginatedata } from "../libs/pagination";
import {
  agregate,
  deleteNote,
  editNote,
  getNoteId,
  getOwnNote,
} from "../controllers/note.controller";
const router = Router();

router.post("/agregate", verifyToken, agregate);

router.put("/:id/note-edit", verifyToken, editNote);

router.delete("/:id/delete-note", verifyToken, deleteNote);

router.get("/:id/get-note", verifyToken, getNoteId);

router.get("/:userId/get-own-note", verifyToken, getPaginatedata, getOwnNote);

export default router;
