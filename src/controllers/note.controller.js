import Note from "../models/Note";

export const agregate = async (req, res) => {
  try {
    const { userId, title, description } = req.body;

    if (
      userId === undefined ||
      title === undefined ||
      description === undefined
    )
      return res.status(203).json({
        message: "Completa  los campos para agregar una nota nueva",
        status: false,
      });

    const { _id, name } = req.user;
    const note = await Note({
      userId,
      title,
      description,
      userTkn: { _id, name },
    });

    const newNote = await note.save();
    res
      .status(200)
      .json({ message: "Nueva nota creada", status: true, newNote });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const editNote = async (req, res) => {
  try {
    const { id } = req.params;
    const { title, description } = req.body;

    if (id === undefined)
      return res.status(203).json({
        message: "Envia  id  de la nota en el parametro",
        status: false,
      });

    const noteEdit = await Note({
      title,
      description,
    });

    Note.updateOne(
      { _id: id },
      {
        $set: req.body
      },
      {new: true}
    )

      .then((data) =>
        res.json({
          message: "Nota actualizada correctamente",
          status: true,
          noteEdit,
          data,
        })
      )
      .catch((error) =>
        res.status({
          message: "Error al  actualizar la nota, intentalo nuevamente",
          status: false,
          error,
        })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};

export const deleteNote = async (req, res) => {
  try {
    const { id } = req.params;
    Note.deleteOne({ _id: id })
      .then((data) =>
        res.json({
          message: "Nota  eliminada correctamente",
          status: true,
          Note,
          data,
        })
      )
      .catch((error) =>
        res.status({
          message: "Error al  eliminada la nota, intentalo nuevamente",
          status: false,
          error,
        })
      );
  } catch (err) {
    res.status(500).json(err);
  }
};

export const getNoteId = async (req, res) => {
  try {
    const { id } = req.params;

    Note.findById(id)
      .then((data) =>
        res.json({
          message: "Nota encontrada, mostrando",
          status: true,
          Note,
          data,
        })
      )
      .catch((error) =>
        res.json({ message: "Error al buscar la nota", status: false, error })
      );
    return;
  } catch (err) {
    res.status(500).json(err);
  }
};

export const getOwnNote = async (req, res) => {
  try {
    const { userId } = req.params;
    const cant = await Note.count();
    Note.find({ userId })
      .skip(req.body.skippag)
      .limit(req.body.limit)
      .sort({ _id: -1 })
      .then(async (data) => {
        res.json({
          message: "Mostrando mis notas...",
          status: true,
          data,
          pagination: {
            pag: req.params.pag,
            perpage: req.body.limit,
            pags: Math.ceil(cant / req.body.limit),
          },
        });
      })
      .catch((error) => {
        console.log(error.message);
        res.json({
          message: "Error al cargar mis notas",
          status: false,
          error,
        });
      });
  } catch (e) {
    res.send(e.message);
  }
};
