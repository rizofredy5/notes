import User from "../models/User";
import config from "../config";
import jwt from "jsonwebtoken";

export const register = async (req, res) => {
  try {
    const { email, password, name } = req.body;

    if (email === undefined || password === undefined || name === undefined)
      return res.status(403).json({
        message: "Complete todos los campos para  registrarse",
        status: false,
      });

    var usuario = await User.findOne({ email });
    if (usuario)
      return res.status(203).json({
        message: "Email registrado, ingresa otro email",
        status: false,
      });

    const pass = await User.encryptPassword(password);
    const newUser = new User({
      name,
      email,
      password: pass,
    });

    const user = await newUser.save();
    res.status(200).json({ message: "Registro exitoso", status: true, user });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    if (email === undefined || password === undefined) {
      return res.status(203).json({
        message: "Completa los campos para iniciar sesion",
        status: false,
      });
    }

    var dateUser = await User.findOne({ email });
    if (!dateUser) {
      return res
        .status(203)
        .json({
          message: "Este email no se encuentra registrado",
          status: false,
        });
    }

    const validContra = await User.comparePassword(password, dateUser.password);
    if (!validContra) {
      return res
        .status(203)
        .json({ message: "Contraseña  incorrecta", status: true });
    }

    const token = jwt.sign(
      {
        id: dateUser.id,
        name: dateUser.name,
        email: dateUser.email,
        isAdmin: dateUser.isAdmin,
      },
      config.SECRET,
      { expiresIn: "365d" }
    );

    const newUser = {
      id: dateUser.id,
      token_temp: token,
    };

    const resp = await User.updateOne({ _id: dateUser.id }, newUser);
    if (!resp) {
      return res.status(203).json({
        message: "Error al iniciar sesión",
        status: false,
        token,
        usuario: {
          id: dateUser.id,
          name: dateUser.name,
          email: dateUser.email,
          isAdmin: dateUser.isAdmin,
        },
      });
    }

    return res.status(200).json({
      message: "Bienvenido",
      status: true,
      token,
      usuario: {
        id: dateUser.id,
        name: dateUser.name,
        email: dateUser.email,
        isAdmin: dateUser.isAdmin,
      },
    });
  } catch (err) {
    res.status(500).json(err);
  }
};

export const logout = async (req, res) => {
  try {
    const { email } = req.body;
    const recentUser = await User.findOne({ email });
    User.updateUser({ token_temp: 0, id: recentUser.usuario._id });
    return res.json({ status: true, message: "Vuelve pronto." });
  } catch (error) {
    res.status(500).json(error);
  }
};