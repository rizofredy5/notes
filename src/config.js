import { config } from "dotenv";

config();

export default {
  port: process.env.PORT || "",
  userdb: process.env.USERDB || "",
  SECRET: process.env.SECRET || "contra, token",
  MONGODB_URL: process.env.MONGODB_URL || "",
};
