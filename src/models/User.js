import mongoose from "mongoose";
import bcrypt from "bcrypt";

const UserSchema = mongoose.Schema(
  {
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String,
      unique: true
    },
    name: {
      type: String,
      unique: true
    },
    isAdmin: {
      type: String,
      default: false,
    },
  },
  { timestamps: true }
);

UserSchema.statics.encryptPassword = async (password) => {
  const salt = await bcrypt.genSalt(10);
  return await bcrypt.hash(password, salt);
};

UserSchema.statics.comparePassword = async (password, receivedPassword) => {
  return await bcrypt.compare(password, receivedPassword);
};

export default mongoose.model("Usuario", UserSchema);
