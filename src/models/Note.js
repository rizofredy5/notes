import mongoose from "mongoose";

const NoteSchema = mongoose.Schema(
  {
    userId: {
      type: String,
    },
    title: {
      type: String,
    },
    description: {
      type: String,
    },
    userTkn:{
      _id:  String,
      name: String
    }
  },
  { timestamps: true }
);

export default mongoose.model("Note", NoteSchema);
