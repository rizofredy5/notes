import express from "express";
import morgan from "morgan";
import cors from "cors";
import config from "./config";

//? Import routes
import UseRoutes from "./routes/user.routes";
import NoteRoutes from "./routes/note.routes";

//TODO: Base de datos
const initDB = require("./database/mongodtbase");
initDB();

const app = express();

//TODO: SETTING
app.get("/", (req, res) => {
  res.send("hola,  probando desde aqui 🫡");
});

app.use(cors());
app.set("port", config.port);

//TODO: MIDDLEWARE
app.use(express.json());
app.use(morgan("dev"));

app.use((req, res, next) => {
  console.log("Time:", new Date());
  next();
});

//TODO: Routes
app.use("/api/users", UseRoutes);
app.use("/api/notes", NoteRoutes);

export default app;
