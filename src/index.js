import app from "./app";

const main = () => {
  app.listen(app.set("port"));
  console.log(`Server on port ${app.get("port")}`);
};

main();

export default app;
