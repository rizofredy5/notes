const mongoose = require ('mongoose');
require('dotenv').config()

const DB_URI = process.env.MONGODB_URL
mongoose.set('strictQuery', true); //TODO: IMPORTANTE

module.exports = () => {

    const connect = () => {
        mongoose.connect (
            DB_URI,
            {
                keepAlive: true,
                useNewUrlParser: true,
                useUnifiedTopology: true
            },
            (error) => {
                if(error) {
                    console.log('ERROR DE DB');
                } else {
                    console.log('Conexion correcta 🃏');
                }
            }
        )
    }
    connect();
}